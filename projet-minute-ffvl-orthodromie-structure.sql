SET client_encoding = 'UTF8';
DROP SCHEMA "ffvl_orthodromie" CASCADE;
CREATE SCHEMA "ffvl_orthodromie";

CREATE TABLE "ffvl_orthodromie"."villes_france" (
    "ville_id" integer NOT NULL,
    "ville_departement" character varying,
    "ville_slug" character varying,
    "ville_nom" character varying,
    "ville_nom_simple" character varying,
    "ville_nom_reel" character varying,
    "ville_nom_soundex" character varying,
    "ville_nom_metaphone" character varying,
    "ville_code_postal" character varying,
    "ville_commune" character varying,
    "ville_code_commune" character varying NOT NULL,
    "ville_arrondissement" integer,
    "ville_canton" character varying,
    "ville_amdi" integer,
    "ville_population_2010" integer,
    "ville_population_1999" integer,
    "ville_population_2012" integer,
    "ville_densite_2010" integer,
    "ville_surface" double precision,
    "ville_longitude_deg" double precision,
    "ville_latitude_deg" double precision,
    "ville_longitude_grd" character varying,
    "ville_latitude_grd" character varying,
    "ville_longitude_dms" character varying,
    "ville_latitude_dms" character varying,
    "ville_zmin" integer,
    "ville_zmax" integer
);
ALTER TABLE ONLY "ffvl_orthodromie"."villes_france"
    ADD CONSTRAINT "villes_france_free_pkey" PRIMARY KEY ("ville_id");
GRANT USAGE ON SCHEMA "ffvl_orthodromie" TO "etudiants-slam";
GRANT SELECT ON TABLE "ffvl_orthodromie"."villes_france" TO "etudiants-slam";
